# -*- coding: utf-8 -*-

import scrapy
import json
from bs4 import BeautifulSoup
import wikipedia
import urllib
from termcolor import cprint
import re

CONST_MAX_OUT_LINKS = 5
CONST_MAX_LINK_TO_CRAWL = 10
wikipedia.set_lang('fa')
CONST_START_PAGE = wikipedia.page('سعدی')
CONST_START_URL = str(CONST_START_PAGE.url)
index_to_url = {0: CONST_START_URL}
url_dict = {}
parsed = 1


def correct_url(url):
    url = url.replace('//', 'http://')
    url = "http://fa.wikipedia.org" + url
    return url


def is_it_valid_url(url, id, link):
    title = str(link.get('title'))

    if ':' in title or title[0].isdigit(): return False
    if 'edit' in url or ':' in url or '#' in url: return False
    if '//' in url and 'https' in url: return False
    if '(' in url: return False
    # cprint(url, 'red')
    if 'wiki' not in url: return False
    digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹']
    for digit in digits:
        if digit in id:
            return False

    return True


class MySpider(scrapy.Spider):
    name = "wiki_fa_spider"
    allowed_domains = ["fa.wikipedia.org"]
    wikipedia.set_lang('fa')
    start_urls = (CONST_START_URL,)
    url_dict[CONST_START_URL] = [0, CONST_START_PAGE.title, CONST_START_PAGE.summary, CONST_START_PAGE.content]

    def parse(self, response):
        if len(url_dict) >= CONST_MAX_LINK_TO_CRAWL:
            # self.close('wiki_fa_spider', 'nothing')
            return
        index = len(url_dict)

        my_url = str(response.url)
        my_id = urllib.parse.unquote(my_url.split('/')[-1]).replace('_', ' ')
        soap = BeautifulSoup(response.text, 'html.parser')

        main_context = soap.find('div', attrs={'class': 'mw-content-rtl'})
        table = main_context.find('table', attrs={'class': re.compile('infobox')})

        if table is not None: table.extract()
        for table in main_context.find_all('table'): table.extract()

        for extra in main_context.find_all('div'): extra.extract()
        for script in main_context.find_all('script'): script.extract()

        summary = main_context.p.text
        content = str(main_context.text)

        page_info = [index, my_id, summary, content]
        url_dict[my_url] = page_info

        with open('pages_info/' + str(index) + '.json', 'w') as outfile:
            json.dump(page_info, outfile)
        with open('pages_info/' + str(index) + '.txt', 'w') as outfile:
            outfile.write('TITLE: ' + my_id + '\n\n\nSUMMARY:\n' + summary + '\n\n\nCONTENT:\n' + content)
        process_bar(index, CONST_MAX_LINK_TO_CRAWL, length=20)

        if len(url_dict) >= CONST_MAX_LINK_TO_CRAWL:
            return
        cprint(str(len(url_dict)) + ', ' + str(CONST_MAX_LINK_TO_CRAWL), 'red')

        link_number = 0
        for link in main_context.find_all('a'):
            url = str(link.get('href'))
            id = urllib.parse.unquote(url.split('/')[-1]).replace('_', ' ')
            if not is_it_valid_url(url, id, link): continue
            link_number += 1
            if CONST_MAX_OUT_LINKS <= link_number: break
            url = correct_url(url)
            index_to_url[len(index_to_url)] = url

        global parsed
        while parsed < len(index_to_url):
            parsed += 1
            # cprint(parsed, 'yellow')
            if len(url_dict) >= CONST_MAX_LINK_TO_CRAWL:
                return
            yield scrapy.Request(url=index_to_url[parsed],callback=self.parse)
            # cprint(index_to_url[parsed], 'yellow')
        else:
            return
