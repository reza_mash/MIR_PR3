import scrapy
import json
from bs4 import BeautifulSoup
import urllib
from Utility import process_bar
import re
import html2text
# import sys
# from crawler import crawler

CONST_MAX_OUT_LINKS = 6
CONST_MAX_LINK_TO_CRAWL = 30
CONST_START_URL = 'https://fa.wikipedia.org/wiki/سعدی'
index_to_url = {0: CONST_START_URL}
url_dict = {}
parsed = 1


def correct_url(url):
    url = url.replace('//', 'http://')
    url = "http://fa.wikipedia.org" + url
    return url


def is_it_valid_url(url, id, link):
    title = str(link.get('title'))

    if ':' in title or title[0].isdigit(): return False
    if 'edit' in url or ':' in url or '#' in url: return False
    if '//' in url and 'https' in url: return False
    if '(' in url: return False
    # cprint(url, 'red')
    if 'wiki' not in url: return False
    digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹']
    for digit in digits:
        if digit in id:
            return False

    return True


class MySpider(scrapy.Spider):
    name = "wiki_fa_spider"
    allowed_domains = ["fa.wikipedia.org"]
    start_urls = (CONST_START_URL,)

    def parse(self, response):
        if len(url_dict) >= CONST_MAX_LINK_TO_CRAWL:
            return

        index = len(url_dict)

        my_url = str(response.url)
        my_id = urllib.parse.unquote(my_url.split('/')[-2 if my_url.endswith('/') else -1]).replace('_', ' ')
        soap = BeautifulSoup(response.text, 'html.parser')

        main_context = soap.find('div', attrs={'class': 'mw-content-rtl'})
        table = main_context.find('table', attrs={'class': re.compile('infobox')})

        if table is not None: table.extract()
        for table in main_context.find_all('table'): table.extract()

        for extra in main_context.find_all('div'): extra.extract()
        for script in main_context.find_all('script'): script.extract()

        summary = main_context.p.text
        content = str(main_context.text)
        infobox = str(table.text)

        link_number = 0
        out_link_ids=[]
        for link in main_context.find_all('a'):
            url = str(link.get('href'))
            id = urllib.parse.unquote(url.split('/')[-2 if url.endswith('/') else -1]).replace('_', ' ')
            out_link_ids.append(id)
            if not is_it_valid_url(url, id, link): continue
            link_number += 1
            url = correct_url(url)
            if link_number <= CONST_MAX_OUT_LINKS and len(url_dict) < CONST_MAX_LINK_TO_CRAWL:
                index_to_url[len(index_to_url)] = url

        page_info = {'index': str(index), 'id': my_id, 'summary': summary, 'content': content,
                     'out_links': out_link_ids, 'cluster_id': -1, 'cluster_name': '', 'page_rank': 0, 'infobox':infobox}
        url_dict[my_url] = page_info

        with open('pages_info/' + str(index) + '.json', 'w') as outfile:
            json.dump(page_info, outfile)
        with open('pages_info/' + str(index) + '.txt', 'w') as outfile:
            outfile.write('TITLE: ' + my_id + '\n\n\nSUMMARY:\n' + summary + '\n\n\nCONTENT:\n' + content)
        process_bar(index + 1, CONST_MAX_LINK_TO_CRAWL, length=50)

        if len(url_dict) >= CONST_MAX_LINK_TO_CRAWL:
            return

        global parsed
        while parsed < len(index_to_url):
            parsed += 1
            if len(url_dict) >= CONST_MAX_LINK_TO_CRAWL:
                return
            yield scrapy.Request(url=index_to_url[parsed],callback=self.parse)
            # cprint(index_to_url[parsed], 'yellow')
        else:
            return
