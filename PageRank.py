import numpy as np
from termcolor import cprint
CONST_MULTIPLE_TIMES = 100
CONST_ZARIB = 1000
from Utility import process_bar

def normalize(inp):
    sum = 0
    for i in range(len(inp)):
        sum += inp[i]
    for i in range(len(inp)):
        inp[i] /= sum
    return inp


def page_rank(graph_matrix, alpha, to_print, docs):
    # graph_matrix = np.array(graph_matrix)
    if alpha > 1.0:
        cprint('Error:\n\t\t⍺ should not be more than 1.0', 'red')
        return
    if alpha >= 0.8:
        cprint('Warning:\n\t\t⍺ is better to be lower than 0.8', 'yellow')
    if alpha <= 0.0:
        cprint('Error:\n\t\t⍺ should be more than 0.0 to converge be guaranteed', 'red')
        return
    if alpha < 0.01:
        cprint('Warning:\n\t\t⍺ is better to be more than 0.01', 'yellow')

    n = len(graph_matrix)
    ans = []
    out_link_num = []
    for i in range(n):
        ans.append(1 / n)
        temp = 0
        for j in range(n):
            if graph_matrix[i][j] != 0:
                temp += 1
        out_link_num.append(temp)
    ans = np.array(ans)

    random_walk = [[None] * n for _ in range(n)]
    for i in range(n):
        for j in range(n):
            if graph_matrix[i][j] == 0:
                random_walk[i][j] = (alpha / n)
            else:
                random_walk[i][j] = (1 / out_link_num[i]) * (1 - alpha) + (alpha / n)

    random_walk = np.array(random_walk)

    for i in range(CONST_MULTIPLE_TIMES):
        ans = ans.dot(random_walk)
        ans = normalize(ans)
        process_bar(i+1, CONST_MULTIPLE_TIMES, 10)

    for i in range(n):
        ans[i] *= n * CONST_ZARIB

    if to_print:
        print()
        for i in range(n):
            cprint(docs[i]['id'] + ': ' + str(ans[i]), 'blue')

    return ans


def make_graph_matrix(docs, id_to_index):
    n = len(docs)
    graph_matrix = [[None] * n for _ in range(n)]
    for i in range(n):
        for j in range(n):
            graph_matrix[i][j] = 0
    for i in range(n):
        for id in docs[i]['out_links']:
            if id in id_to_index:
                graph_matrix[i][id_to_index[id]] = 1
    return graph_matrix






# graph_matrix = [[0, 1, 1], [0, 0, 0], [1, 0, 0]]
# page_rank(graph_matrix, 0.005, True)
