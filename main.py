# -*- coding: utf-8 -*-

from elasticsearch import Elasticsearch
from scrapy.crawler import CrawlerProcess
import CrawlerCodes
import json
import glob, os
from collections import Counter
from Kmeans import cluster
from PageRank import make_graph_matrix, page_rank

from graphviz import Digraph
command = ''
doc_counter = 0
es = Elasticsearch()
while command != 'exit':
    print('Enter your command')
    command_with_args = input().split(' ')
    command = command_with_args[0]
    if command == 'draw':
        dot = Digraph(comment='The round table')
        prog = 0
        os.chdir("pages_info")
        doc_counter = len(glob.glob("*.json"))
        docs = [None] * doc_counter
        index_to_id = [''] * doc_counter
        id_to_index = {}
        print('Loading data...')
        for file in glob.glob("*.json"):
            with open(file) as doc_file:
                doc = json.load(doc_file)
                docs[int(doc['index'])] = doc
                index_to_id[int(doc['index'])] = doc['id']
                id_to_index[doc['id']] = int(doc['index'])
                dot.node(doc['id'], doc['id'])
                prog += 1
                CrawlerCodes.process_bar(prog, doc_counter, 10)
        os.chdir("..")
        print("\nDrawing Graph...")
        # prog = 0
        graph_matrix = make_graph_matrix(docs, id_to_index)
        for i in range(doc_counter):
            for j in range(doc_counter):
                if graph_matrix[i][j] == 1:
                    dot.edge(index_to_id[i], index_to_id[j])
                    # prog += 1
                    # CrawlerCodes.process_bar(prog, doc_counter * doc_counter, 20)
        dot.render('round-table.gv', view=True)

    if command == 'crawl':
        n, out_link = -1, -1
        address = ''
        state = 'crawl'
        for cm in command_with_args:
            if cm == 'crawl':
                state = 'crawl'
            elif cm == '-n':
                state = 'n'
            elif cm == '-o':
                state = 'out links'
            elif cm == '-a':
                state = 'address'
            else:
                if state == 'n':
                    n=int(cm)
                elif state == 'out links':
                    out_link = int(cm)
                elif state == 'address':
                    address = cm
            if n != -1:
                CrawlerCodes.CONST_MAX_LINK_TO_CRAWL = n
            if address != '':
                CrawlerCodes.MySpider.start_urls = ('https://fa.wikipedia.org/wiki/' + address,)
            if out_link != -1:
                CrawlerCodes.CONST_MAX_OUT_LINKS = out_link

        process = CrawlerProcess({
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
            'LOG_ENABLED': False
        })
        print('Crawl process initiated...')
        process.crawl(CrawlerCodes.MySpider)
        process.start()
        print('\nCrawl process terminated.')
    if command == 'index':
        if len(command_with_args) != 2 or command_with_args[1] not in ['--all', '--delete']:
            print('index usage:\n\t\tindex --all:\t index documents\n\t\tindex --delete:\t delete all indexes')
            continue
        if command_with_args[1] == '--delete':
            es.indices.delete(index='wiki_fa')
        if command_with_args[1] == '--all':
            print('Start indexing...')
            os.chdir("pages_info")
            doc_counter = len(glob.glob("*.json"))
            prog = 0
            for file in glob.glob("*.json"):
                with open(file) as doc_file:
                    doc = json.load(doc_file)
                    res = es.index(index='wiki_fa', doc_type='web_page', id=int(doc['index']), body=doc)
                    prog += 1
                    CrawlerCodes.process_bar(prog, doc_counter, 30)
            os.chdir("..")
            print('\nIndex completed.')

    if command == 'cluster':
        l = 10
        state = 'cluster'
        for cm in command_with_args:
            if cm == 'cluster':
                state = 'cluster'
            elif cm == '-l':
                state = 'L'
            else:
                if state == 'L':
                    l = int(cm)

        vector_space = Counter()
        for i in range(doc_counter):
            temp = {}
            terms = es.termvectors(index='wiki_fa', doc_type='web_page', id=i, fields=['content'])['term_vectors']['content']['terms']
            for term in terms.keys():
                temp[term] = terms[term]['term_freq']
            vector_space |= Counter(temp)

        cluster_name, cluster_id = cluster(vector_space, es, doc_counter, 'wiki_fa', 'web_page', l=l)

        print('Start updating index...')
        os.chdir("pages_info")
        doc_counter = len(glob.glob("*.json"))
        prog = 0
        for file in glob.glob("*.json"):
            with open(file) as doc_file:
                doc = json.load(doc_file)
                temp_id = int(doc['index'])
                doc['cluster_id'] = int(cluster_id[temp_id])
                doc['cluster_name'] = cluster_name[cluster_id[temp_id]]
                doc['page_rank'] = es.get(index='wiki_fa', doc_type='web_page', id=temp_id)['_source']['page_rank']
                res = es.index(index='wiki_fa', doc_type='web_page', id=int(doc['index']), body=doc)
                prog += 1
                CrawlerCodes.process_bar(prog, doc_counter, 30)
        os.chdir("..")
        print('\nUpdate completed.')

    if command == 'page':
        state = 'page rank'
        alpha = 0.3
        to_print = False
        for cm in command_with_args:
            if cm in ['page', 'rank']:
                state = 'page rank'
            elif cm == '-a':
                state = 'alpha'
            elif cm == '-p':
                to_print = True
            else:
                if state == 'alpha':
                    alpha = float(cm)

        prog = 0
        os.chdir("pages_info")
        doc_counter = len(glob.glob("*.json"))
        docs = [None] * doc_counter
        id_to_index = {}
        print('Loading data...')
        for file in glob.glob("*.json"):
            with open(file) as doc_file:
                doc = json.load(doc_file)
                docs[int(doc['index'])] = doc
                id_to_index[doc['id']] = int(doc['index'])
                prog += 1
                CrawlerCodes.process_bar(prog, doc_counter, 10)
        os.chdir("..")
        print("\nCalculating pages' rank...")
        pagerank = page_rank(make_graph_matrix(docs, id_to_index), alpha, to_print, docs)
        if pagerank is None:
            continue
        print('\nStart updating index...')
        os.chdir("pages_info")
        doc_counter = len(glob.glob("*.json"))
        prog = 0
        for file in glob.glob("*.json"):
            with open(file) as doc_file:
                doc = json.load(doc_file)
                temp_id = int(doc['index'])
                doc['page_rank'] = pagerank[temp_id]
                doc['cluster_id'] = es.get(index='wiki_fa', doc_type='web_page', id=temp_id)['_source']['cluster_id']
                doc['cluster_name'] = es.get(index='wiki_fa', doc_type='web_page', id=temp_id)['_source']['cluster_name']
                res = es.index(index='wiki_fa', doc_type='web_page', id=int(doc['index']), body=doc)
                prog += 1
                CrawlerCodes.process_bar(prog, doc_counter, 30)
        os.chdir("..")
        print('\nUpdate completed.')


    if command == 'search':
        if len(command_with_args) == 0:
            print('Search Usage:\t\tsearch -q your query -t title query -s summary query -sb (summary boost value)')
            continue
        query, title, summary = '', '', ''
        content_boost, summary_boost, title_boost = 0, 0, 0
        should = []
        pagerank = False
        cluster_id = -1
        cm = 'search'
        for cm in command_with_args:
            if cm == 'search':
                state = 'search'
            elif cm == '-q':
                state = 'query'
            elif cm == '-t':
                state = 'title'
            elif cm == '-s':
                state = 'summary'
            elif cm == '-sb':
                state = 'summary boost'
            elif cm == '-tb':
                state = 'title boost'
            elif cm == '-qb':
                state = 'query boost'
            elif cm == '-c':
                state = 'cluster'
            elif cm == '-p':
                pagerank = True
            else:
                if state == 'query':
                    if query == '':
                        query = cm
                    else:
                        query += ' ' + cm
                elif state == 'title':
                    if title == '':
                        title = cm
                    else:
                        title += ' ' + cm
                elif state == 'summary':
                    if summary == '':
                        summary = cm
                    else:
                        summary += ' ' + cm
                elif state == 'query boost':
                    query_boost = int(cm)
                elif state == 'title boost':
                    title_boost = int(cm)
                elif state == 'summary boost':
                    summary_boost = int(cm)
                elif state == 'cluster':
                    cluster_id = int(cm)

        must_clause = {}
        print(cluster_id)
        if cluster_id != -1:
            must_clause = {"term": {"cluster_id": cluster_id}}
        value_factors = {}
        if pagerank:
            value_factors = {"field": "page_rank", "modifier": "log1p"}

        if query != '':
            should.append({"match": {
                "content": {
                    "query": query,
                    "boost": content_boost
                }}})
        if summary != '':
            should.append({"match": {
                "summary": {
                    "query": summary,
                    "boost": summary_boost
                }}})
        if title != '':
            should.append({"match": {
                "id": {
                    "query": title,
                    "boost": title_boost
                }}})

        function_score = {"query": {
            "bool": {
                "must": must_clause,
                "should": should
            }
        }, "score_mode": "sum"}
        if pagerank:
            function_score["field_value_factor"] = value_factors

        query_body = {
            "query": {
                "function_score": function_score
            }}

        print(query_body)
        result = es.search(index='wiki_fa', doc_type='web_page', body=query_body, size=10)

        for hit in result['hits']['hits']:
            print("%(id)s " % hit["_source"])
