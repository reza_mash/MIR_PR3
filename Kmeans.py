from collections import Counter
import random
import math
import operator
CONST_TRIES_PER_K = 1
CONST_LANDA = 100
CONST_TITLE_LENGTH = 5

#
# sagzan = Counter()
# sagzan[1] = 10
# sagzan[2] = 100
# print(sagzan)

def calculate_cost(k, vector_space, term_counter):
    cluster_size = Counter()

    random_points = []
    cluster_name = [''] * k
    cluster_id = [-1] * len(term_counter)
    old_cost = math.inf
    new_cost = 0
    for i in range(k):
        temp = Counter()
        for x in vector_space:
            temp[x] = random.randint(0, vector_space[x])
        random_points.append(temp)

    while old_cost != new_cost:
        old_cost = new_cost
        new_cost = 0
        assign = {}
        assign_to_id = {}
        for i in range(k):
            assign[i] = []
            assign_to_id[i] = []
        for j in range(len(term_counter)):
            min_dis = math.inf
            index = -1
            for i in range(k):
                distance = dis(term_counter[j], random_points[i])
                if distance < min_dis:
                    index = i
                    min_dis = distance
            assign[index].append(term_counter[j])
            assign_to_id[index].append(j)
            new_cost += min_dis
        for i in range(k):
            random_points[i] = centroid_point(assign[i])

    for i in range(k):
        for doc_id in assign_to_id[i]:
            cluster_id[doc_id] = i

    for i in range(k):
        cluster_size[i] = len(assign_to_id[i])

    N11 = {}
    for term in dict(vector_space).keys():
        temp_counter = Counter()
        for i in range(k):
            temp = 0
            for doc_id in assign_to_id[i]:
                if term_counter[doc_id][term] != 0:
                    temp += 1
            temp_counter[i] = temp
        N11[term] = temp_counter

    docs_per_term = Counter()
    for term in dict(vector_space).keys():
        temp = 0
        for i in range(len(term_counter)):
            if term_counter[i][term] != 0:
                temp += 1
        docs_per_term[term] = temp

    n = len(term_counter)
    for i in range(k):
        term_score = {}
        for term in dict(vector_space).keys():
            n11 = N11[term][i]
            n10 = docs_per_term[term] - n11
            n01 = cluster_size[i] - n11
            n00 = n - n01 - n10 - n11
            n11 += 1
            n10 += 1
            n01 += 1
            n00 += 1
            n1_ = n10 + n11
            n_0 = n10 + n00
            n0_ = n00 + n01
            n_1 = n11 + n01
            score = (n11/n) * math.log(n*n11/n1_/n_1, 2) + n01/n * math.log(n*n01/n0_/n_1, 2) +\
                    n10/n * math.log(n*n10/n1_/n_0, 2) + n00/n * math.log(n*n00/n0_/n_0)
            term_score[term] = -score
        sorted_score = sorted(term_score.items(), key=operator.itemgetter(1))
        name = ''
        for j in range(CONST_TITLE_LENGTH):
            name += sorted_score[j][0] + ' '
        cluster_name[i] = name

    return new_cost, cluster_name, cluster_id


def centroid_point(vectors):
    ans = Counter()
    temp = Counter()
    for vector in vectors:
        temp += vector
    for x in temp:
        ans[x] = temp[x] / len(vectors)
    return ans


def dis(vector1, vector2):
    ans = 0
    temp = vector1 - vector2
    for x in temp:
        ans += abs(temp[x])
    temp = vector2 - vector1
    for x in temp:
        ans += abs(temp[x])
    return ans


def cluster(vector_space, es, doc_counter, index, doc_type, l):
    if l == -1:
        l = 100
    term_counter = []
    for i in range(doc_counter):
        temp = {}
        terms = es.termvectors(index=index, doc_type=doc_type, id=i, fields=['content'])['term_vectors']['content']['terms']
        for term in terms.keys():
            temp[term] = terms[term]['term_freq']
        term_counter.append(Counter(temp))

    cluster_name = []
    cluster_id = []

    cluster_name_to_return = []
    cluster_id_to_return = []
    old_cost = math.inf
    for k in range(1, l+1):
        cost = math.inf
        for i in range(CONST_TRIES_PER_K):
            temp, cluster_name, cluster_id = calculate_cost(k, vector_space, term_counter)
            if temp < cost:
                cost = temp

        cost += k * CONST_LANDA
        print('Cost for ' + str(k) + ' clusters: ' + str(cost))
        if old_cost <= cost:
            break
        old_cost = cost
        cluster_name_to_return = cluster_name
        cluster_id_to_return = cluster_id

    print(cluster_id_to_return)
    print(cluster_name_to_return)
    return cluster_name_to_return, cluster_id_to_return








# print(centroid_point([Counter({'a': 2, 'c': 7}), Counter({'a': 4, 'b': 3})]))

# print(dis(Counter({'a': 2, 'c': 7}), Counter({'a': 4, 'b': 3})))