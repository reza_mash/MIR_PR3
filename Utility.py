from termcolor import cprint
import sys
blocks = [' ', '▏', '▎', '▍', '▌', '▋', '▊', '▉', '█']


def process_bar(prog, whole, length):
    # prog += 1
    percent = int(prog * 100 / whole)
    i = int(prog * length * 8/ whole) % 8
    prog = int(prog * length / whole)
    print('\r[', end='')
    cprint('█' * prog, 'green', end='')
    if prog < length: cprint(blocks[i], 'green', end='')
    print(' ' * (length - prog - 1) + '] ' + str(percent) + '% ', end='')
    # print(str(prog)+ ','+str(whole) + str(length))
    sys.stdout.flush()



